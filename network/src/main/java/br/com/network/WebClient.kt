package br.com.network

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object WebClient {
    fun retrofit(url: String): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .baseUrl(url)
        .client(OkHttpClient.Builder().build())
        .build()

    inline fun <reified T> service(url: String): T = retrofit(url).create(T::class.java)
}