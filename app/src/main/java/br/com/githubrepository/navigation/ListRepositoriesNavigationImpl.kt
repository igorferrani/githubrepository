package br.com.githubrepository.navigation

import android.content.Context
import br.com.listrepositories.navigation.ListRepositoriesNavigation

object ListRepositoriesNavigationImpl : ListRepositoriesNavigation {
    override fun toFeatureInOtherModule(context: Context) {
        // Navigate to Activity in Other Module
    }
}