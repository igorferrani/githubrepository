package br.com.githubrepository

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.githubrepository.navigation.ListRepositoriesNavigationImpl
import br.com.listrepositories.presentation.ui.MainActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // open Activity in module :listrepositories
        startActivity(Intent(this, MainActivity::class.java))
    }
}