package br.com.githubrepository

import android.app.Application
import br.com.githubrepository.navigation.ListRepositoriesNavigationImpl
import br.com.listrepositories.di.ListRepositoriesDI
import br.com.listrepositories.navigation.ListRepositoriesNavigationBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CustomApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        ListRepositoriesNavigationBuilder.Builder(ListRepositoriesNavigationImpl)

        startKoin {
            androidLogger()
            androidContext(this@CustomApplication)
        }
        ListRepositoriesDI.initKoin()
    }
}