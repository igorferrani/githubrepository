package br.com.listrepositories.domain.repository

import br.com.listrepositories.domain.model.Repository

interface ListRepositoriesRepository {
    suspend fun getDataRepositoriesGitHub(page: Int): List<Repository>
}