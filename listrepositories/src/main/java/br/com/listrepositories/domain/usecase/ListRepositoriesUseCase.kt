package br.com.listrepositories.domain.usecase

import br.com.listrepositories.domain.model.Repository
import br.com.listrepositories.domain.repository.ListRepositoriesRepository

class ListRepositoriesUseCase(private val repository: ListRepositoriesRepository) {
    private var page: Int = 1

    suspend fun getList(nextPage: Boolean = false): List<Repository> {
        if (nextPage) page++
        return repository.getDataRepositoriesGitHub(this.page)
    }

    fun getPage(): Int = page
}