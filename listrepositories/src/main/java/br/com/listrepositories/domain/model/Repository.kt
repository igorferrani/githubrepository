package br.com.listrepositories.domain.model

import android.os.Parcelable

interface Repository : Parcelable {
    val id: Int
    val name: String
    val stargazersCount: Int
    val forksCount: Int
    val owner: Owner
}

interface Owner {
    val ownerName: String
    val photo: String
}