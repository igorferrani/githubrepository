package br.com.listrepositories.di

import androidx.room.Room
import br.com.listrepositories.repository.database.RepositoriesDatabase
import br.com.listrepositories.domain.repository.ListRepositoriesRepository
import br.com.listrepositories.repository.remote.ListRepositoriesRepositoryImpl
import br.com.listrepositories.repository.remote.ListRepositoriesRemoteDataSource
import br.com.listrepositories.repository.ServiceConstant
import br.com.listrepositories.repository.ServiceConstant.DATABASE_NAME
import br.com.listrepositories.domain.usecase.ListRepositoriesUseCase
import br.com.listrepositories.presentation.viewmodel.ListRepositoriesViewModel
import br.com.network.WebClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object ListRepositoriesDI {
    private val modules = module {
        viewModel { ListRepositoriesViewModel(repositoriesUseCase = get()) }
        factory { ListRepositoriesUseCase(repository = get()) as ListRepositoriesUseCase }
        factory { ListRepositoriesRepositoryImpl(remote = get(), local = get()) as ListRepositoriesRepository }
        single { WebClient.service<ListRepositoriesRemoteDataSource>(ServiceConstant.URL) }
        single {
            Room.databaseBuilder(
                get(),
                RepositoriesDatabase::class.java,
                DATABASE_NAME
            ).build()
        }
    }

    fun initKoin() = loadKoinModules(modules)
}