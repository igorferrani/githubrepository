package br.com.listrepositories.navigation

import android.content.Context

interface ListRepositoriesNavigation {
    fun toFeatureInOtherModule(context: Context)
}