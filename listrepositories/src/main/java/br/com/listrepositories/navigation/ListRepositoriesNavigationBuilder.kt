package br.com.listrepositories.navigation

object ListRepositoriesNavigationBuilder {
    var instance: ListRepositoriesNavigation? = null

    class Builder(private val navigation: ListRepositoriesNavigation) {
        fun create() {
            instance = navigation
        }
    }
}