package br.com.listrepositories.repository.remote

import br.com.listrepositories.repository.response.DataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ListRepositoriesRemoteDataSource {
    @GET("search/repositories")
    suspend fun getListRepositories(
        @Query("sort") sort: String = "stars",
        @Query("page") page: Int = 1,
        @Query("q", encoded = true) query: String = "language:kotlin",
    ): DataResponse
}