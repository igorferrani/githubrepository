package br.com.listrepositories.repository.remote

import androidx.room.withTransaction
import br.com.listrepositories.domain.model.Repository
import br.com.listrepositories.domain.repository.ListRepositoriesRepository
import br.com.listrepositories.repository.database.ListRepositoryEntity
import br.com.listrepositories.repository.database.OwnerEntity
import br.com.listrepositories.repository.database.RepositoriesDatabase

class ListRepositoriesRepositoryImpl(
    private val remote: ListRepositoriesRemoteDataSource,
    private val local: RepositoriesDatabase
) :
    ListRepositoriesRepository {

    override suspend fun getDataRepositoriesGitHub(page: Int): List<Repository> {
        return try {
            val result = remote.getListRepositories(page = page)
            val list = result.items
            saveLocal(list)
            getLocal()
        } catch (exception: Exception) {
            val localList = getLocal()
            localList.ifEmpty {
                throw exception
            }
        }
    }

    private suspend fun saveLocal(listRepositories: List<Repository>) {
        val list = mutableListOf<ListRepositoryEntity>()
        listRepositories.map {
            list.add(
                ListRepositoryEntity(
                    it.id,
                    it.name,
                    it.stargazersCount,
                    it.forksCount,
                    OwnerEntity(
                        it.owner.ownerName,
                        it.owner.photo
                    )
                )
            )
        }
        local.withTransaction {
            local.repositoriesDAO().insertListRepository(list)
        }
    }

    private suspend fun getLocal(): List<Repository> {
        return local.withTransaction {
            local.repositoriesDAO().getAllRepositories()
        }
    }
}