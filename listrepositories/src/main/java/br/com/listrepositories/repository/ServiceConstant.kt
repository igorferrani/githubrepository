package br.com.listrepositories.repository

object ServiceConstant {
    const val URL: String = "https://api.github.com/"
    const val DATABASE_NAME: String = "GITHUB_REPOSITORIES"
    const val DATABASE_VERSION: Int = 1
}