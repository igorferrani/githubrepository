package br.com.listrepositories.repository.database

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import br.com.listrepositories.domain.model.Owner
import br.com.listrepositories.domain.model.Repository
import kotlinx.parcelize.Parcelize

@Entity(tableName = "Repository")
@Parcelize
data class ListRepositoryEntity(
    @PrimaryKey
    override val id: Int,
    override val name: String,
    override val stargazersCount: Int,
    override val forksCount: Int,
    @Embedded
    override val owner: OwnerEntity
) : Parcelable, Repository

@Parcelize
data class OwnerEntity(
    override val ownerName: String,
    override val photo: String
) : Parcelable, Owner
