package br.com.listrepositories.repository.database

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.listrepositories.repository.ServiceConstant.DATABASE_VERSION

@Database(version = DATABASE_VERSION, entities = [ListRepositoryEntity::class], exportSchema = false)
abstract class RepositoriesDatabase : RoomDatabase() {
    abstract fun repositoriesDAO(): ListRepositoriesDAO
}