package br.com.listrepositories.repository.response

import android.os.Parcelable
import br.com.listrepositories.domain.model.Owner
import br.com.listrepositories.domain.model.Repository
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class DataResponse(
    @SerializedName("items") val items: List<RepositoryResponse>
) : Parcelable

@Parcelize
data class RepositoryResponse(
    @SerializedName("id") override val id: Int,
    @SerializedName("name") override val name: String,
    @SerializedName("stargazers_count") override val stargazersCount: Int,
    @SerializedName("forks_count") override val forksCount: Int,
    @SerializedName("owner") override val owner: OwnerResponse
) : Parcelable, Repository

@Parcelize
data class OwnerResponse(
    @SerializedName("avatar_url") override val photo: String,
    @SerializedName("login") override val ownerName: String
) : Parcelable, Owner