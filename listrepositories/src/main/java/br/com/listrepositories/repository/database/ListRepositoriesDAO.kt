package br.com.listrepositories.repository.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ListRepositoriesDAO {
    @Query("SELECT * FROM Repository ORDER BY stargazersCount DESC")
    fun getAllRepositories(): List<ListRepositoryEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertListRepository(repository: List<ListRepositoryEntity>)
}