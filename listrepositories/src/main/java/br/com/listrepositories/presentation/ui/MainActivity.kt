package br.com.listrepositories.presentation.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.listrepositories.R
import br.com.listrepositories.databinding.ActivityListRepositoriesBinding
import br.com.listrepositories.domain.model.Repository
import br.com.listrepositories.presentation.viewmodel.ListRepositoriesEvent
import br.com.listrepositories.presentation.viewmodel.ListRepositoriesState
import br.com.listrepositories.presentation.viewmodel.ListRepositoriesViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    private val adapter: RepositoryListAdapter = RepositoryListAdapter()
    private val repositoriesViewModel: ListRepositoriesViewModel by viewModel()
    private lateinit var binding: ActivityListRepositoriesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListRepositoriesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()
        setupObservablesState()
        setupObservablesEvent()
        fetchData()
    }

    private fun fetchData() {
        repositoriesViewModel.getInitList()
    }

    private fun setupObservablesState() {
        repositoriesViewModel.state.observe(this) {
            when (it) {
                is ListRepositoriesState.ListRepositoriesSuccess -> {
                    updateList(it.list)
                }
                is ListRepositoriesState.ListRepositoriesError -> {
                    it.exception.message?.let { message ->
                        showError(message)
                    }
                }
            }
        }
    }

    private fun showError(message: String) {
        Toast.makeText(
            this, message,
            Toast.LENGTH_LONG
        ).show()
    }

    private fun setupObservablesEvent() {
        repositoriesViewModel.event.observe(this) {
            when (it) {
                is ListRepositoriesEvent.ShowLoading -> {
                    showLoading()
                }
                is ListRepositoriesEvent.HideLoading -> {
                    hideLoading()
                }
            }
        }
    }

    private fun showLoading() {
        binding.pbLoading.announceForAccessibility(resources.getString(R.string.app_message_loading))
        binding.pbLoading.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        binding.pbLoading.visibility = View.GONE
    }

    private fun updateList(list: List<Repository>) {
        adapter.updateList(list)
    }

    private fun setupRecyclerView() {
        binding.rvListItemRepositories.adapter = adapter
        binding.rvListItemRepositories.layoutManager = LinearLayoutManager(this)
        binding.rvListItemRepositories.addOnScrollListener(rvScrollListener())
    }

    private fun rvScrollListener(): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                recyclerView.layoutManager?.itemCount?.let {
                    val lastVisibleItemPosition =
                        (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                    if (it > ZERO_ITEM_LIST && lastVisibleItemPosition >= it.minus(ONE_ITEM_LIST)) {
                        repositoriesViewModel.getNextPageList()
                    }
                }
            }
        }
    }

    companion object {
        const val ZERO_ITEM_LIST: Int = 0
        const val ONE_ITEM_LIST: Int = 1
    }
}