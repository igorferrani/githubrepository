package br.com.listrepositories.presentation.viewmodel

sealed class ListRepositoriesEvent {
    object ShowLoading : ListRepositoriesEvent()
    object HideLoading : ListRepositoriesEvent()
}