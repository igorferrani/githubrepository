package br.com.listrepositories.presentation.viewmodel

import br.com.listrepositories.domain.model.Repository

sealed class ListRepositoriesState {
    data class ListRepositoriesSuccess(val list: List<Repository>) : ListRepositoriesState()
    data class ListRepositoriesError(val exception: Exception) : ListRepositoriesState()
}