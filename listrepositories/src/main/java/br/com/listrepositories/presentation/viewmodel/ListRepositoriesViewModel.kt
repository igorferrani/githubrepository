package br.com.listrepositories.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.listrepositories.domain.usecase.ListRepositoriesUseCase
import kotlinx.coroutines.launch

class ListRepositoriesViewModel(private val repositoriesUseCase: ListRepositoriesUseCase) : ViewModel() {
    private var isInit = true
    private val _state: MutableLiveData<ListRepositoriesState> = MutableLiveData()
    val state: LiveData<ListRepositoriesState> = _state

    private val _event: MutableLiveData<ListRepositoriesEvent> = MutableLiveData()
    val event: LiveData<ListRepositoriesEvent> = _event

    private fun getList(nextPage: Boolean = false) {
        viewModelScope.launch {
            try {
                _event.postValue(ListRepositoriesEvent.ShowLoading)
                val result = repositoriesUseCase.getList(nextPage)
                isInit = false
                _state.postValue(ListRepositoriesState.ListRepositoriesSuccess(result))
            } catch (exception: Exception) {
                _state.postValue(ListRepositoriesState.ListRepositoriesError(exception))
            } finally {
                _event.postValue(ListRepositoriesEvent.HideLoading)
            }
        }
    }

    fun getInitList() {
        if (!isInit) return
        getList()
    }

    fun getNextPageList() {
        getList(true)
    }

    fun isInit(): Boolean = isInit
}