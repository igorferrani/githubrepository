package br.com.listrepositories.presentation.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.listrepositories.R
import br.com.listrepositories.databinding.ListRepositoriesItemBinding
import br.com.listrepositories.domain.model.Repository
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

class RepositoryListAdapter : RecyclerView.Adapter<RepositoryListAdapterViewHolder>() {

    private val list = mutableListOf<Repository>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryListAdapterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_repositories_item, parent, false)
        return RepositoryListAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: RepositoryListAdapterViewHolder, position: Int) {
        holder.binding(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(list: List<Repository>) {
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }
}

class RepositoryListAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding = ListRepositoriesItemBinding.bind(itemView)

    fun binding(item: Repository) {
        binding.tvListItemRepositoriesProjectName.text = item.name
        binding.tvListItemRepositoriesStargazersCount.text = item.stargazersCount.toString()
        binding.tvListItemRepositoriesOwnerName.text = item.owner.ownerName
        binding.tvListItemRepositoriesForkCount.text = item.forksCount.toString()
        Glide
            .with(itemView.context)
            .load(item.owner.photo)
            .apply(RequestOptions.circleCropTransform())
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .into(binding.ivPhoto)
    }
}