package br.com.listrepositories

import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import br.com.listrepositories.presentation.ui.MainActivity
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    val appContext = InstrumentationRegistry.getInstrumentation().targetContext

    @Test
    fun useAppContext() {
        // Context of the app under test.
        assertEquals("br.com.listrepositories.test", appContext.packageName)
    }

    @Test
    fun Ao_abrir_activity_deve_exibir_lista_de_repositorios() {
        val intent = Intent(appContext, MainActivity::class.java)
        intent.flags = FLAG_ACTIVITY_NEW_TASK
        appContext.startActivity(intent)
    }
}