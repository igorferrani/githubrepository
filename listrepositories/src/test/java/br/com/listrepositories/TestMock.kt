package br.com.listrepositories

import br.com.listrepositories.repository.database.OwnerEntity
import br.com.listrepositories.repository.database.ListRepositoryEntity
import br.com.listrepositories.repository.response.DataResponse
import br.com.listrepositories.repository.response.OwnerResponse
import br.com.listrepositories.repository.response.RepositoryResponse

object TestMock {
    val listRepositoryEntityMock: List<ListRepositoryEntity> = arrayListOf(
        ListRepositoryEntity(
            1,
            "repo 1",
            123,
            321,
            OwnerEntity(
                "username",
                "https://photo.png"
            )
        )
    )

    val repositoriesMock: DataResponse = DataResponse(
        arrayListOf(
            RepositoryResponse(
                1,
                "repo 1",
                123,
                321,
                OwnerResponse(
                    "https://photo.png",
                    "username"
                )
            )
        )
    )

    val repositoriesEmptyMock: DataResponse = DataResponse(arrayListOf())
}