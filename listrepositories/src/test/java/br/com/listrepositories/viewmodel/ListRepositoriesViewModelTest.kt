package br.com.listrepositories.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.listrepositories.CoroutineTestRule
import br.com.listrepositories.TestMock
import br.com.listrepositories.domain.usecase.ListRepositoriesUseCase
import br.com.listrepositories.presentation.viewmodel.ListRepositoriesEvent
import br.com.listrepositories.presentation.viewmodel.ListRepositoriesState
import br.com.listrepositories.presentation.viewmodel.ListRepositoriesViewModel
import br.com.listrepositories.repository.database.ListRepositoryEntity
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ListRepositoriesViewModelTest {

    private val useCase: ListRepositoriesUseCase = mockk()
    private val listRepositoryEntityMock = TestMock.listRepositoryEntityMock
    private val viewModel = ListRepositoriesViewModel(useCase)

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val scope = CoroutineTestRule()

    @Test
    fun `Quando chamar o metodo getList, deve retornar estado de sucesso com lista populada e ocultar o loading`() {
        runBlockingTest {
            // Given
            coEvery { useCase.getList(false) }.returns(listRepositoryEntityMock)

            // When
            viewModel.getInitList()

            // Then
            assertEquals(ListRepositoriesState.ListRepositoriesSuccess(listRepositoryEntityMock), viewModel.state.value)
            assertEquals(ListRepositoriesEvent.HideLoading, viewModel.event.value)
            assertFalse(viewModel.isInit())
        }
    }

    @Test
    fun `Quando chamar o metodo getList, deve retornar estado de sucesso com lista vazia e ocultar o loading`() {
        runBlockingTest {
            // Given
            coEvery { useCase.getList(false) }.returns(arrayListOf<ListRepositoryEntity>())

            // When
            viewModel.getInitList()

            // Then
            assertEquals(
                ListRepositoriesState.ListRepositoriesSuccess(arrayListOf<ListRepositoryEntity>()),
                viewModel.state.value
            )
            assertEquals(ListRepositoriesEvent.HideLoading, viewModel.event.value)
        }
    }

    @Test
    fun `Quando chamar o metodo getList e houver excecoes, deve retornar estado de erro com Throw Exception e ocultar o loading`() {
        runBlockingTest {
            // Given
            val expectedException = Exception("Error")
            coEvery { useCase.getList(false) } throws expectedException

            // When
            viewModel.getInitList()

            // Then
            assertEquals(
                ListRepositoriesState.ListRepositoriesError(expectedException),
                viewModel.state.value
            )
            assertEquals(ListRepositoriesEvent.HideLoading, viewModel.event.value)
        }
    }

    @Test
    fun `Ao instanciar viewmodel, isInit sera igual a True`() {
        runBlockingTest {
            // Given
            coEvery { useCase.getList(false) }.returns(listRepositoryEntityMock)

            // Then
            assert(viewModel.isInit())
        }
    }

    @Test
    fun `Quando chamar o metodo getNextPageList, deve retornar estado de sucesso com lista populada, ocultar o loading e isInit=false`() {
        runBlockingTest {
            // Given
            coEvery { useCase.getList(true) }.returns(listRepositoryEntityMock)

            // When
            viewModel.getNextPageList()

            // Then
            assertEquals(ListRepositoriesState.ListRepositoriesSuccess(listRepositoryEntityMock), viewModel.state.value)
            assertEquals(ListRepositoriesEvent.HideLoading, viewModel.event.value)
            assertFalse(viewModel.isInit())
        }
    }

    @Test
    fun `Quando chamar o metodo getNextPageList e houver excecoes, deve retornar estado de erro com Throw Exception e ocultar o loading`() {
        runBlockingTest {
            // Given
            val expectedException = Exception("Error")
            coEvery { useCase.getList(true) } throws expectedException

            // When
            viewModel.getNextPageList()

            // Then
            assertEquals(
                ListRepositoriesState.ListRepositoriesError(expectedException),
                viewModel.state.value
            )
            assertEquals(ListRepositoriesEvent.HideLoading, viewModel.event.value)
        }
    }
}