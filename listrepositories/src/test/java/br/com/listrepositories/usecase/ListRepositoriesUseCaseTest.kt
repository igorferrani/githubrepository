package br.com.listrepositories.usecase

import br.com.listrepositories.domain.repository.ListRepositoriesRepository
import br.com.listrepositories.domain.usecase.ListRepositoriesUseCase
import br.com.listrepositories.TestMock
import br.com.listrepositories.repository.database.ListRepositoryEntity
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Test

@ExperimentalCoroutinesApi
class ListRepositoriesUseCaseTest {
    private val repository: ListRepositoriesRepository = mockk()
    private val listRepositoryEntityMock: List<ListRepositoryEntity> = TestMock.listRepositoryEntityMock

    @Test
    fun `Quando chamar metodo getList deve retornar uma lista populada`() {
        runBlockingTest {
            // Given
            val useCase = ListRepositoriesUseCase(repository)
            coEvery { repository.getDataRepositoriesGitHub(1) }.returns(listRepositoryEntityMock)

            // When
            val result = useCase.getList()

            // Then
            assertEquals(listRepositoryEntityMock, result)
        }
    }

    @Test
    fun `Quando chamar metodo getList deve retornar uma lista vazia`() {
        runBlockingTest {
            // Given
            val useCase = ListRepositoriesUseCase(repository)
            coEvery { repository.getDataRepositoriesGitHub(1) }.returns(arrayListOf<ListRepositoryEntity>())

            // When
            val result = useCase.getList()

            // Then
            assertEquals(arrayListOf<ListRepositoryEntity>(), result)
        }
    }

    @Test
    fun `Quando chamar metodo getList deve retornar um throw Exception`() {
        assertThrows(Exception::class.java) {
            runBlockingTest {
                // Given
                val useCase = ListRepositoriesUseCase(repository)
                coEvery { repository.getDataRepositoriesGitHub(1) } throws Exception("Error")

                // Then
                useCase.getList()
            }
        }
    }

    @Test
    fun `Quando chamar metodo getList passando nextPage=true deve retornar uma lista populada e page=2`() {
        runBlockingTest {
            // Given
            val useCase = ListRepositoriesUseCase(repository)
            coEvery { repository.getDataRepositoriesGitHub(2) }.returns(listRepositoryEntityMock)

            // When
            val result = useCase.getList(true)

            // Then
            assertEquals(listRepositoryEntityMock, result)
            assertEquals(useCase.getPage(), 2)
        }
    }
}