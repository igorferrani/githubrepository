package br.com.listrepositories.repository

import androidx.room.withTransaction
import br.com.listrepositories.R
import br.com.listrepositories.domain.repository.ListRepositoriesRepository
import br.com.listrepositories.TestMock
import br.com.listrepositories.repository.database.ListRepositoriesDAO
import br.com.listrepositories.repository.database.RepositoriesDatabase
import br.com.listrepositories.repository.database.ListRepositoryEntity
import br.com.listrepositories.repository.remote.ListRepositoriesRemoteDataSource
import br.com.listrepositories.repository.remote.ListRepositoriesRepositoryImpl
import br.com.listrepositories.repository.response.DataResponse
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.slot
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class ListRepositoriesRepositoryTest {
    private val remote: ListRepositoriesRemoteDataSource = mockk()
    private val local: RepositoriesDatabase = mockk()
    private val listRepositoriesDAO: ListRepositoriesDAO = mockk()

    private val repository: ListRepositoriesRepository = ListRepositoriesRepositoryImpl(remote, local)
    private val listRepositoryEntityMock: List<ListRepositoryEntity> = TestMock.listRepositoryEntityMock
    private val repositoriesMock: DataResponse = TestMock.repositoriesMock
    private val repositoriesEmptyMock: DataResponse = TestMock.repositoriesEmptyMock

    @Before
    fun beforeTest() {
        MockKAnnotations.init(this)
        mockkStatic("androidx.room.RoomDatabaseKt")

        // Mock withTransaction for Room
        val transactionLambda = slot<suspend () -> R>()
        coEvery { local.withTransaction(capture(transactionLambda)) } coAnswers { transactionLambda.captured.invoke() }

        coEvery { local.repositoriesDAO() }.returns(listRepositoriesDAO)
    }

    @Test
    fun `Quando chamar metodo getDataRepositoriesGitHub com page=1 deve retornar uma lista populada`() {
        runBlockingTest {
            // Given
            coEvery { remote.getListRepositories() }.returns(repositoriesMock)
            coEvery { local.repositoriesDAO().insertListRepository(listRepositoryEntityMock) } returns Unit
            coEvery { local.repositoriesDAO().getAllRepositories() }.returns(listRepositoryEntityMock)

            // When
            val result = repository.getDataRepositoriesGitHub(1)

            // Then
            assertEquals(listRepositoryEntityMock, result)
        }
    }

    @Test
    fun `Quando chamar metodo getDataRepositoriesGitHub com page=2 deve retornar uma lista populada`() {
        runBlockingTest {
            // Given
            coEvery { remote.getListRepositories() }.returns(repositoriesMock)
            coEvery { local.repositoriesDAO().insertListRepository(listRepositoryEntityMock) } returns Unit
            coEvery { local.repositoriesDAO().getAllRepositories() }.returns(listRepositoryEntityMock)

            // When
            val result = repository.getDataRepositoriesGitHub(2)

            // Then
            assertEquals(listRepositoryEntityMock, result)
        }
    }

    @Test
    fun `Quando chamar metodo getDataRepositoriesGitHub com page=1 e existir um Throw Exception deve retornar uma lista populada em cache`() {
        runBlockingTest {
            // Given
            coEvery { remote.getListRepositories() } throws Exception("Error")
            coEvery { local.repositoriesDAO().insertListRepository(listRepositoryEntityMock) } returns Unit
            coEvery { local.repositoriesDAO().getAllRepositories() }.returns(listRepositoryEntityMock)

            // When
            val result = repository.getDataRepositoriesGitHub(1)

            // Then
            assertEquals(listRepositoryEntityMock, result)
        }
    }

    @Test
    fun `Quando chamar metodo getDataRepositoriesGitHub e nao existir dados no remoto e nem local, deve retornar uma lista vazia`() {
        runBlockingTest {
            // Given
            coEvery { remote.getListRepositories() }.returns(repositoriesEmptyMock)
            coEvery { local.repositoriesDAO().insertListRepository(arrayListOf()) } returns Unit
            coEvery { local.repositoriesDAO().getAllRepositories() }.returns(arrayListOf())

            // When
            val result = repository.getDataRepositoriesGitHub(1)

            // Then
            assertEquals(arrayListOf<ListRepositoryEntity>(), result)
        }
    }
}