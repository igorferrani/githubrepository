![Logo GitHub](./logo_github.png)

# GitHub Repositories

> Projeto tem como objetivo entregar uma funcionalidade no qual liste reposítorios do GitHub filtrados pela linguagem kotlin e ordenado por estrelas.

## Requisitos
- Utilizar API: https://api.github.com/search/repositories?q=language:kotlin&sort=stars&page=1
- Exibir nome do repo, quantidade de estrelas, quantidade de fork, foto e nome do autor
- Scroll infinito
- Testes unitários
- Linguagem Kotlin
- Android Architecture Components
- Testes de UI usando Espresso
- Rx ou Coroutines
- Cache de imagens e da API
- Suportar mudanças de orientação das telas sem perder o estado

### Tecnologias utilizadas

- Room Database
- Linguagem Kotlin
- Testes unitários com JUnit + Mocck
- Clean Archtecture (base arquitetura Exagonal)
- Design Patterns (GoF): Adapter, Singleton, Builder
- Coroutines
- LiveData
- Acessibilidade (diretrizes WCAG)